<?php
/**
 * Script to get Employee payout Data from Airtable and 
 * Post it to client server with basic Auth
 * 
 * Example Airtable Base(Public): https://airtable.com/shrp9CK5JLPzyhU2z
 *
 * PHP version 8.0.3
 *
 * LICENSE: N/A
 *
 * @author     Original Author gyana111@gmail.com
 * @copyright  N/A
 * @version    1.0
 */


// get all the payouts
$payouts = json_decode(getPayouts());

// add the payout records to an array to calculate and add more records 
// if more than 100 rows present in airtable
$allPayouts = $payouts->records;

// Airtable api gives maximum 100 records in a call so if there are more than 100 records,
// we have to call the api with offset value that comes with the get api call to airtable. 
// if offset present we call the airtable api again with offset value
while (isset($payouts->offset)) {
	$payouts = json_decode(getPayouts($payouts->offset));

	// save the retrived data to predefined records array
	$allPayouts = array_merge($allPayouts,$payouts->records);
}
// arrange the payouts as an array with names as key and total payment as values
$totalPayments = array();
foreach ($allPayouts as $payout) {

	if(isset($totalPayments[$payout->fields->{'Employee Name'}])) {
        $totalPayments[$payout->fields->{'Employee Name'}] +=  (float)$payout->fields->Amount;
	} else {
        $totalPayments += array($payout->fields->{'Employee Name'} => (float)$payout->fields->Amount);
	}
}

// convert the float amount to dollar amount rounded to it's nearest cent value
$totalPayments = array_map(function($value) { return round($value, 2); }, $totalPayments);

// convert the payments to json for sending with the post payload
$data = json_encode($totalPayments);

// make the post request to send data to client server
$username = 'gyana111@gmail.com';
$password = '4P#Wle5h4*';
$url = 'https://auth.da-dev.us/devtest1';

$ch = curl_init($url);
// curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36 OPR/77.0.4054.254');
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data))                                                               
);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);

$result=curl_exec ($ch);
$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close ($ch);

print_r($status_code);
print_r($result);


// function to get records from airtable
function getPayouts($offset=false) {

	$airtableAPIKey = "keyVvR4Xse2xEiAVz";

	// we are only bringing the name and amount value from the table as we don't need more data. It will save fetch time.
	// The data is sort by pay date
	$url = "https://api.airtable.com/v0/app33ofeGVzDDgbkz/Payouts?&fields%5B%5D=Employee%20Name&fields%5B%5D=Amount&sort%5B0%5D%5Bfield%5D=Pay%20Date&maxRecords=10&offset=". $offset;
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$headers = array(
	   "Accept: application/json",
	   "Authorization: Bearer ". $airtableAPIKey,
	);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$response = curl_exec($ch);
	curl_close($ch);

	return $response;
}